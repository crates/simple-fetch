This is a really simple http(s) client library.

It only provides two public functions:

`pub fn fetch_url(url: &str) -> Result<Box<Read>>` fetches a URL and
returns, on success, a std::io::Read.

`pub fn fetch_url_string(url: &str) -> Result<String>` does the same but
returns the whole body as a `String`.

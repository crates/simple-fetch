extern crate mio_httpc;

use std::io::Read;
use mio_httpc::CallBuilder;
use std::error::Error;
use std::fmt;

#[derive(Debug)]
pub struct FetchError {
    pub message: String,
    cause: Option<Box<dyn Error + Sync + Send>>
}

impl FetchError {
    fn new(message: String) -> FetchError {
        FetchError {
            message,
            cause: None,
        }
    }

    fn with_cause(message: String, cause: Box<dyn Error + Sync + Send>) -> FetchError {
        FetchError {
            message,
            cause: Some(cause),
        }
    }

    pub fn cause(&self) -> &Option<Box<dyn Error + Sync + Send>> {
        &self.cause
    }
}

impl fmt::Display for FetchError {
    fn fmt(&self, f: &mut fmt::Formatter) -> fmt::Result {
        write!(f, "{}", self.message)
    }
}

impl From<std::io::Error> for FetchError
{
    fn from(err: std::io::Error) -> FetchError {
        FetchError::with_cause(format!("{}", err), Box::new(err))
    }
}

impl Error for FetchError {
}

pub type FetchResult<T> = std::result::Result<T, FetchError>;

pub fn fetch_url(url: &str) -> FetchResult<Box<dyn Read>> {
    mio_fetch_url(url).map_err(|e| { 
        FetchError::new(format!("Error fetching url: {}", e))
    })
}

struct Data {
    data: Vec<u8>,
    pos: usize,
}

impl Read for Data {
    fn read(&mut self, buf: &mut [u8]) -> std::io::Result<usize> {
        let r = (&self.data[self.pos..]).read(buf);
        if let Ok(i) = r {
            self.pos += i;
        }
        r
    }
}

fn mio_fetch_url(url: &str) -> std::result::Result<Box<dyn Read>, mio_httpc::Error> {
    let (response, body) = 
        CallBuilder::get().timeout_ms(10000).chunked_max_chunk(1024 * 1024 * 10).url(url)?.exec()?;

    if response.status != 200 {
        //let message = format!("Downloading {} failed: {}", url, response.status);
        return Err(mio_httpc::Error::Other("Downloading failed"));
    }

    Ok(Box::new(Data { data: body, pos: 0 }))
}


pub fn fetch_url_string(url: &str) -> FetchResult<String> {
    let mut result = fetch_url(url)?;
    let mut s = String::new();
    result.read_to_string(&mut s)?;
    Ok(s)
}

#[cfg(test)]
mod tests {
    use ::fetch_url_string;

    #[test]
    fn fetch_http() {
        let _res = fetch_url_string("http://neverssl.com").unwrap();
    }

    #[cfg(any(feature = "with-rustls", feature = "with-openssl", feature = "with-native"))]
    #[test]
    fn fetch_https() {
        let _res = fetch_url_string("https://www.debian.org").unwrap();
    }

    #[test]
    #[should_panic]
    fn fetch_http_error() {
        let _res = fetch_url_string("http://127.0.0.1:6").unwrap();
    }

    #[test]
    fn fetch_http_error_result() {
        let res = fetch_url_string("http://127.0.0.1:6");
        println!("{}", res.as_ref().unwrap_err());
    }
}
